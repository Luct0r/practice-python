#!/usr/bin/python3
# Simple program that finds if a given number is prime or not.

def isPrime():
    number = int(input("Please enter a number to check: "))

    if number % 2 == 0 and number != 2:
        print("This number isn't prime, it's even!")
        print("Even numbers are divisible by 2.")
    elif number == 1 or number % 3 == 0 or number % 5 == 0 or number % 7 == 0 or number % 9 == 0:
        print("This number isn't prime, it's odd though!")
    elif number % 3 >= 1:
        print("This number is prime!")
    else:
        print("You have entered something wrong...the universe will now cease to exist.")

isPrime()