#!/usr/bin/python3
# Figures out how long until user is 100 years old based on their birthday.

# Need datetime module for current date.
from datetime import datetime

# Parse the date for different paramaters (year, month, day).
dateToday = datetime.now()
dateYear = dateToday.year
dateMonth = dateToday.month
dateDay = dateToday.day

# Create answer function so don't have to repeat code.
def answer():
    print()
    print(name + ", you're %s now and it will be %s years until you're one hundred!" % (birthYear, hundred))
    print()

# Create function for how many years until 100.
def hundred():
    until = 100 - birthYear
    return until

# Intro message and questions for user input.
print("Hi, this program tells you how many years left until you reach 100!")
print()
name = input("Please enter your name: ")
birthday = input("Please enter your birthdate (01/01/1900): ")
# Some error handling so user puts in actual birthday.
while len(birthday) != 10:
    birthday = input("Error! Please enter a valid birthdate: ")
# Parse birthday for parameters (year, month, day).
ageMonth = int(birthday[:2])
ageDay = int(birthday[3:5])
ageYear = int(birthday[-4:])
# IF to handle age calculation after current date.
if dateMonth >= ageMonth and dateDay >= ageDay:
    birthYear = dateYear - ageYear
    hundred = hundred()
    answer()
# ELIF to handle same month but day less than current date.
elif dateMonth == ageMonth and dateDay < ageDay:
    birthYear = (dateYear - ageYear) - 1
    hundred = hundred()
    answer()
# ELSE to handle all other cases.
else:
    birthYear = (dateYear - ageYear) - 1
    hundred = hundred()
    answer()